#include<stdio.h>
#include<stdlib.h>
#include"LinkStack.h"

PLINKSTACK createEmptyStack()
{
	PLINKSTACK s = (PLINKSTACK)malloc(sizeof(LINKSTACK));
	if(s == NULL)
	{
		printf("内存分配失败\n");
	}
	else
	{
		s->top = NULL;
	} 
	return s;
}

bool isEmptyStack(PLINKSTACK s)
{
	return (s->top == NULL);
}

bool push(PLINKSTACK s,dataType x)
{	
	PNODE p = (PNODE)malloc(sizeof(NODE));
	if(p == NULL)
	{
		printf("结点分配失败\n");
		return false;
	}
	else
	{
		p->data = x;
		p->next = s->top;			//链表的下一个结点指向之前的top 第一次为NULL
		s->top = p;					//把top指针指向新的结点p
		return true; 
	}
} 

bool pop(PLINKSTACK s)
{
	if(isEmptyStack(s))
	{
		printf("stack is empty\n");
		return false;
	}
	PNODE p = s->top;
	printf("pop: %d \n",p->data);
	s->top = p->next;
	free(p);
	p = NULL;
	return true;
}

void showStack(PLINKSTACK s)
{
	if(isEmptyStack(s))
	{
		printf("stack is empty\n");
		return ;
	}
	else
	{
		PNODE p = s->top;	
		printf("---\n");
		while(p != NULL)
		{
			printf("%d \n",p->data);
			p = p->next;
		}
		printf("---\n");
	}
}

dataType getTop(PLINKSTACK s)
{
	if(isEmptyStack(s))
	{
		printf("stack is empty\n");
		return NULL;
	}
	else
	{
		return s->top->data;
	}
} 

void setEmpty(PLINKSTACK s)
{
	if(isEmptyStack(s))
	{
		printf("stack is empty");
		return ;
	}
	//释放掉链表 (其实就是没有头结点单链表的销毁)
	PNODE p = s->top;
	PNODE temp = NULL;
	while(p != NULL) 
	{
		temp = p->next;
		free(p);
		p = temp;
	}
	//置空 
	s->top = NULL; 
} 

void destory(PLINKSTACK s)
{
	setEmpty(s);
	free(s);
	s = NULL;
} 

int main()
{
	PLINKSTACK stack = createEmptyStack(); 
	push(stack,1);
	push(stack,2);
	push(stack,3);
	showStack(stack);
	pop(stack);
	showStack(stack);
	printf("top :%d\n", getTop(stack));
	printf("setEmpty……\n");
	setEmpty(stack) ;
	showStack(stack);
	printf("destroy…… \n");
	destory(stack);
	return 0;
}
 
