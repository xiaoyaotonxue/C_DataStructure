#ifndef LinkedList_H
#define LinkedList_H
typedef struct Node
{
	//数据域 
	int data;
	//指针域 
	struct Node *pNext;
}NODE,*PNODE;
//NODE 等价于 struct Node
//*PNODE 等价于  struct Node *

PNODE Create_Node(void);								//创建 （尾插法）
void destory_Node(PNODE pHead);							//销毁 
PNODE clear_Node(PNODE pHead);							//释放所有结点 (保留头指针)
void print(PNODE p);									//顺序打印
int length_Node(PNODE pHead);							//获取node长度 
PNODE find_Node(PNODE pHead, int pos); 					//查找第pos个结点 
bool delete_Node_pos(PNODE pHead, int pos);				//删除第pos个结点 
bool insert_Node(PNODE pHead, int pos, int data); 		//插入（pos>0） 

#endif
