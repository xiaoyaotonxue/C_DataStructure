#include<stdio.h>
#include<stdlib.h>

struct Array
{
	int *pBase;		//指向数组的指针
	int len;		//最大长度
	int cnt;		//当前数组中元素个数 
};

void Init_arr(struct Array *pArr, int len);				//初始数组  

bool Appand_arr(struct Array *pArr, int val);			//在最后添加 
bool Set_arr(struct Array *pArr, int pos,int val);		//对指定下标赋值 
bool Insert_arr(struct Array *pArr,int pos,int val);	//根据下标插入 原来的网后移
bool Delete_arr(struct Array *pArr,int pos);			//删除下标位置的值 

void Show_arr(struct Array *pArr);						//打印 
void Get_arr(struct Array *pArr,int pos);				//根据下标打印 

void Init_arr(struct Array *pArr, int len)
{
	pArr->pBase = (int *)malloc(sizeof(int)*pArr->len);
	if(NULL == pArr->pBase)
	{
		printf("分配内存失败!");
		exit(-1);
	}
	else
	{
		pArr->cnt = 0;
		pArr->len = len;
	}
	return ;
} 

bool Appand_arr(struct Array *pArr, int val)
{
	if(pArr->cnt == pArr->len){
		printf("the array is full");
		return false;
	}
	pArr->pBase[pArr->cnt++]=val;			//找到当前位置的下一个并赋值给它
	return true; 
} 

bool Set_arr(struct Array *pArr,int pos,int val)
{
	if(pos<0 || pos>pArr->len-1)		//pos 与下标对应 不能超出这个范围 
	{
		printf("out of array");
		return false;
	}
	if(pArr->pBase[pos] == NULL)
	{
		pArr->cnt++;
	}
	pArr->pBase[pos] = val;
	return true;
}

bool Insert_arr(struct Array *pArr,int pos,int val)
{
	if(pos<0 || pos>pArr->len-1)		//pos 与下标对应 不能超出这个范围 
	{
		printf("out of array");
		return false;
	}
	if(pArr->cnt == pArr->len)
	{
		printf("array has full");
		return false;
	}
	//pos后面全部后移一位 
	for(int i=pArr->cnt; i>=pos; i--)
	{
		pArr->pBase[i+1] = pArr->pBase[i];
	}
	pArr->pBase[pos] = val;
	pArr->cnt++;
	return true;
}

bool Delete_arr(struct Array *pArr,int pos)
{
	if(pArr->cnt == 0) {
		printf("array is empty");
		return false;
	}
	if(pos<0 || pos>pArr->len-1)		//pos 与下标对应 不能超出这个范围 
	{
		printf("out of array");
		return false;
	}
	for(int i=pos;i<=pArr->cnt-1;i++)  
        pArr->pBase[i]=pArr->pBase[i+1];  
    pArr->cnt--;  
    return true;  
} 

void Show_arr(struct Array *pArr)
{
	int i;
	for(i=0; i<pArr->cnt; i++) 
		printf("%d \n",pArr->pBase[i]);
} 

void Show_arr(struct Array *pArr,int pos)
{
	printf("%d \n", pArr->pBase[pos]) ;
} 

int main()
{
	struct Array arr;
	Init_arr(&arr,8);
	
	Appand_arr(&arr,1);
	Appand_arr(&arr,2);
	Appand_arr(&arr,4);
	Appand_arr(&arr,5);
	Insert_arr(&arr,2,3);
	Set_arr(&arr,5,6) ;
	Show_arr(&arr);
	Delete_arr(&arr,1);
	printf("---------\n");
	Show_arr(&arr);
}
