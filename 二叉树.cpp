#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode
{
	int data;
	struct TreeNode *lchild;	//左
	struct TreeNode *rchild;	//右 
}BTNode;

int createTree(BTNode **T);
void preOrderBiTree(BTNode *T); 
void treeShow(BTNode *T, int depth);


int createTree(BTNode **T)
{
	int ch;
    scanf("%d",&ch);
    if (ch == -1)
    {
        *T = NULL;
        return 0;
    }
    else
    {
        *T = (BTNode *)malloc(sizeof(BTNode));
        if (T == NULL)
        {
            printf("failed\n");
            return 0;
        }
        else
        {
            (*T)->data = ch;
            printf("输入%d的左子节点：",ch);
            createTree(&((*T)->lchild));
            printf("输入%d的右子节点：",ch);
            createTree(&((*T)->rchild));
        }
    }

    return 1;
} 

void preOrderBiTree(BTNode *T)
{
	if(T == NULL)
	{
		return ;
	}
	else
	{
		printf("%d  ",T->data);
		preOrderBiTree(T->lchild);
		preOrderBiTree(T->rchild);
	}
}

void treeShow(BTNode *T, int depth)
{
	if(T == NULL)
	{
		return ;
	}
	treeShow(T->rchild, depth+1);
	for(int k=0; k<depth; k++)
		printf(" ");
	printf("%d\n", T->data);
	treeShow(T->lchild, depth+1);
}

int main()
{
	BTNode *t;
	createTree(&t);
	preOrderBiTree(t);
	treeShow(t,0);
	return 0;
}
