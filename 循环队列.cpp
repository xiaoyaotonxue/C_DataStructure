#include <stdio.h>
#include <stdlib.h>

typedef struct Queue
{
	int *pBase;			//保存数组的首地址 
	int front;			//指向头结点 
	int rear;			//指向最后一个元素的下一个结点 
} QUEUE;

void initQueue(QUEUE *pQueue) ;			//初始化
bool isEmpty(QUEUE *pQueue);			//空否 
bool isFull(QUEUE *pQueue);				//满否 
void enQueue(QUEUE *pQueue, int value); //入队 (尾)
void outQueue(QUEUE *pQueue, int *pV);	//出队 
void show(QUEUE *pQueue);				//打印 

void initQueue(QUEUE *pQueue)
{
	pQueue->pBase = (int *)malloc(sizeof(int) * 10);	//申请10个int的空间
	if(pQueue ==NULL)
	{
		printf("申请内存失败");
		exit(-1);
	}
	else
	{
		pQueue->front = 0;
		pQueue->rear = 0; 
		return ; 
	}
}

bool isEmpty(QUEUE *pQueue)
{
	if(pQueue->front == pQueue->rear)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool isFull(QUEUE *pQueue)
{
	if((pQueue->rear+1)%10 == pQueue->front)
	{
		return true;
	}
	else
	{
		return false;
	}
} 

void enQueue(QUEUE *pQueue, int value)
{
	if(isFull(pQueue))
	{
		printf("队满");
		exit(-1);
	}
	else
	{
		pQueue->pBase[pQueue->rear] = value;
		pQueue->rear = (pQueue->rear + 1) % 10;
	}
}

void outQueue(QUEUE *pQueue, int *pV)
{
	if(isEmpty(pQueue))
	{
		printf("空");
		exit(-1);
	} 
	else
	{
		*pV = pQueue->pBase[pQueue->front];
		pQueue->front = (pQueue->front + 1)%10;
	}
} 

void show(QUEUE *pQueue)
{
	if(isEmpty(pQueue))
	{
		printf("空");
	}	
	else
	{
		printf("-----\n");
		printf("rear: %d , front %d\n",pQueue->rear, pQueue->front);
		int i = pQueue->front;
		while(i != pQueue->rear)
		{
			printf("%d:%d\n",i,pQueue->pBase[i]);
			i = (i+1)%10;
		}
	}
}

int main(){
	QUEUE q ;
	initQueue(&q);
	for(int i=0; i<9; i++)
	{
		enQueue(&q, i);
	}
	show(&q);
	for(int i=0; i<7; i++)
	{
		int value;
		outQueue(&q, &value);
		printf("-%d\n",value);
	}
	for(int i=0; i<6; i++)
	{
		enQueue(&q, i);
	}
	enQueue(&q,11);
	show(&q);
	return 0;
}
