#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
	//数据域 
	int data;
	//指针域 
	struct Node *pNext;
}NODE,*PNODE;
//NODE 等价于 struct Node
//*PNODE 等价于  struct Node *

PNODE Create_Node(void);								//创建 （尾插法）
void destory_Node(PNODE pHead);							//销毁 
PNODE clear_Node(PNODE pHead);							//释放所有结点 (保留头指针)
void print(PNODE p);									//顺序打印
int length_Node(PNODE pHead);							//获取node长度 
PNODE find_Node(PNODE pHead, int pos); 					//查找第pos个结点 
bool delete_Node_pos(PNODE pHead, int pos);				//删除第pos个结点 
bool insert_Node(PNODE pHead, int pos, int data); 		//插入（pos>0） 


//创建链表(尾插法)
PNODE Create_Node(void) 
{ 
	int len, val;
	PNODE List;
	PNODE pHead = (PNODE)malloc(sizeof(NODE));//分配一个头结点 
	if(NULL == pHead) 
	{
		printf("memory allocation failure");
		exit(-1);
	}
	else
	{
		PNODE pTail = pHead;	//尾指针指向头 
		pHead->pNext = NULL;
		printf("please input the length of list: ");
		scanf("%d",&len);		//链表长度 
		for(int i=0; i<len; i++)
		{
			PNODE p = (PNODE)malloc(sizeof(NODE));
			if(NULL == p)
			{
				printf("memory allocation failure");
				exit(-1);
			}
			else
			{
				printf("please input the value of list: ");
				scanf("%d",&val);
				//尾指针指向p 
				p->data=val;
				pTail->pNext=p;
				p->pNext=NULL;
				pTail=p;
			}
		}
	}
	return pHead; //这里返回的是头结点 不是首结点 
	//如果要首结点就返回 pHead->pNext 
}

//销毁 (过河拆桥)
void destory_Node(PNODE pHead)
{
	if(pHead == NULL)
		return ;
	PNODE temp = NULL;
	while(pHead != NULL)
	{
//		printf("--------\n");
//		print(pHead->data);
		temp = pHead->pNext;	//	先拿到下一个结点 
		free(pHead);
		pHead = temp;
//		if(pHead == NULL){
//			printf("NULL");
//		}
	}
}

//清空 
PNODE clear_Node(PNODE pHead)
{
	if(pHead->pNext == NULL)
		return pHead;
	destory_Node(pHead->pNext);
	pHead->pNext = NULL;
	return pHead;
} 

//顺序打印 
void print(PNODE p)
{
	p = p->pNext;
	while(NULL != p){
		printf("%d \n",p->data);
		p = p->pNext;
	}	
}

//获取链表长度 
int length_Node(PNODE pHead)
{
	//这里的长度是有效data的值 所以头结点不算 
	int len = 0;
	PNODE p = pHead->pNext;
	while(p != NULL)
	{
		len++;
		p = p->pNext;
	} 
	return len;
}

//查找第pos结点 
PNODE find_Node(PNODE pHead, int pos)
{
	int i = 0;
	PNODE p = pHead;
	while(NULL != p && i < pos-1)
	{
		p = p->pNext;
		i++;
	}
	if(p->pNext == NULL || i > pos-1)
	{
		printf("error input pos");
		exit(-1);
	}
	return p->pNext;
}

bool delete_Node_pos(PNODE pHead, int pos)
{
	PNODE pre,cur,next;
	if(pos == 1)
	{
		next = pHead->pNext;
		pHead->pNext = next->pNext;
		free(next);
		next = NULL;
		return true;
	}
	pre = find_Node(pHead, pos-1);
	if(pre->pNext != NULL)
	{
		cur = pre->pNext;
		next = cur->pNext; 
		//pos前一位 指向pos后一位 
		pre->pNext = next;
		//释放pos 
		free(cur);
		cur = NULL;
		return true;
	}
	else
	{
		printf("pos out of node");
	}
	return false;
}

//中间或者结尾插入 
bool insert_Node(PNODE pHead, int pos, int data)
{
	PNODE p = NULL; 
	if(pos == 1){
		p = pHead;
	} 
	else
	{
		//找前一位 
		p = find_Node(pHead,pos-1);
	}
	PNODE pNew = (PNODE)malloc(sizeof(NODE));
	if(pNew == NULL)
	{
		printf("memory allocation failure");
		exit(-1);
	}
	printf("insert %d into Node->%d \n",data,pos);
	//最后结果p(pos-1)->pNew(pos)->q(pos+1原第pos结点) 
	pNew->data = data;
	PNODE q = p->pNext;
	p->pNext = pNew;
	pNew->pNext = q;
	return true;
}

int main()
{
	int pos,value;
	PNODE p = Create_Node();
	print(p);
	
	//销毁
//	destory_Node(p);
	
	//清空 
//	clear_Node(p); 
		
	//长度 
//	printf("length of node is %d \n",length_Node(p));
	
	//查找 
//	printf("please input pos:");
//	scanf ("%d",&pos);
//	PNODE p1 = find_Node(p,pos);
//	printf("%d \n", p1->data);

	//删除
//	printf("please input pos what you want delete:");
//	scanf("%d", &pos);
//	if(delete_Node_pos(p,pos))
//		print(p);
	
	//插入 
//	printf("please input pos and value:");
//	scanf("%d %d", &pos,& value);
//	insert_Node(p,pos,value);
//	print(p);
	
	return 0;
}
