#ifndef LinkStack_H
#define LinkStack_H
typedef int dataType;

typedef struct Node
{
	dataType data;
	struct Node *next;
}NODE,*PNODE;

typedef struct LinkStack
{
	PNODE top;									//栈顶指针	
}LINKSTACK,*PLINKSTACK;

PLINKSTACK createEmptyStack();					//创建一个空栈
bool isEmptyStack(PLINKSTACK s);				//判断是否为空栈 
bool push(PLINKSTACK s, dataType x);  			//入栈 
bool pop(PLINKSTACK s);							//出栈 
void showStack(PLINKSTACK s);					//打印所有 
dataType getTop(PLINKSTACK s);					//获取top 
void setEmpty(PLINKSTACK s);					//置空 
void destory(PLINKSTACK s);						//销毁 

#endif
